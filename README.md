# Python Flask Api Basic 

This python project focuses on Building API with FLask. Project is much smaller version of goodreads.com.

>**Note:** Flask wtforms are not used. This is entirely API project and does not focus on the frontend. However, if frontend is added some other language shall be used.

### Tested On -

* gitpod - python version - 3.12.1

### Aspects covered are as below -
Every branch/tag covers the aspsect that are covered in the Project. 

| Features | tag | branch |
|----------|----------|----------|
| GET, POST, DELETE, UPDATE using simple flask   | v1.0  | v1.0 |
| SQLAlchmey used with sqlite db  | v1.1  | v1.1 |

### Project details 
Project is mini API version of goodreads.com. It has list of books that can be added/updated/deleted. This list is created and maintained by Admin user. Admin is user that has certain powers to add/delete/update list of books.

#### API Detils - 

| Book API url  | Operations | Details | User |
|----------|----------|----------|----------|
|  127.0.0.1:5000/  | GET | takes you home page | Any |
|  127.0.0.1:5000/api/v1/resources/books/all  | GET | get list of all books | Any |
|  127.0.0.1:5000/api/v1/resources/books?id=0  | GET | get book by id | Any |
|  127.0.0.1:5000/api/v1/resources/books/add  | POST | add book to bookslist | Admin |
|  127.0.0.1:5000/api/v1/resources/books/delete?id=0 | DELETE | delte book by id | Admin |
|  127.0.0.1:5000/api/v1/resources/books/update?id=0| PUT | update entry of book by id | Admin |

| User API url  | Operations | Details | User |
|----------|----------|----------|----------|
| /api/v1/resources/users   | GET | get list of all users | Admin |
| /api/v1/resources/users/login  | GET | perform login | Registered user |
| /api/v1/resources/users/register   | GET | register user | Any |

#### DB Details - 

At the moment ```sqlite``` database is used, which consist of only 1 table for the Books. 

Tables created for this application - 

**Bookslist table -** Contains list of all the books. 
| id | title  | author | first_sentence | year_published | comments |
|----------|----------|----------|----------|----------|----------|

**BookReviews table -** Contains comments/reviews of all the books.
| id  | review_content  | book_id |
|----------|----------|----------|


#### Library/Moduels Details - 
Flask, SQLAlchmey, Marshmallow

#### Below is the project structure -
```
python_flask_api_basic/
│
├── README.md
├── .gitignore
├── requirements.txt
│
└── app/
    └──api
       └── __init__.py
       └── app.py

```

Before starting install and activate ```venv```
```
# python3 -m venv venv
# source venv/bin/activate
# pip install -r requirements.txt 
#
# cd /app/api
# python api.py         # Starting the application 
```

Each branch/tag is a release that has covered one of the aspect. 
Please checkout to the branch for every release. Each branch has it's own README.md that provides more information about the release and aspects covered.
```
# git checkout v1.1
``` 

### Important links - 
